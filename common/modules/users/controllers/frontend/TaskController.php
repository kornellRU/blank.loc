<?php


namespace modules\users\controllers\frontend;


use Yii;
use common\modules\users\models\Task;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;



/**
 * TaskController implements the CRUD actions for Task model.
 */
class TaskController extends Controller
{
    /**
     * @inheritdoc
     */

    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }
    /**
     * Lists all Task models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Task::find(),
        ]);
        return $this->render('index', [
        'dataProvider' => $dataProvider,
        ]);
     }

        /**
         * Displays a single Task model.
         * @param integer $id
         * @return mixed
         */

        public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }
        /**
         * Crates a new Task model.
         * If creation is successful, the browser will be redirected to the 'view' page.
         * @return mixed
         */

        public function actionCreate()
    {
        $model = new Task();
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Запись успешно добавлена!');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('create', [
                'model' => $model,
            ]);
        }
    }

        /**
         * Updates an existing Task model.
         * If update is successful, the browser will be redirected to the 'view' page.
         * @param integer $id
         * @return mixed
         */

        public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            Yii::$app->session->setFlash('success', 'Данные успешно изменены!');
            return $this->redirect(['view', 'id' => $model->id]);
        } else {
            return $this->render('update', [
                'model' => $model,
            ]);
        }
    }
        /**
         * Deletes an existing Task model.
         * If deletion is successful, the browser will be redirected to the 'index' page.
         * @param integer $id
         * @return mixed
         */

        public function actionDelete($id)
    {
        $model = $this->findModel($id);
        Yii::$app->redis->set($model->name, Task::WORKER_DELETE);
        $model->delete();
        Yii::$app->session->setFlash('success', 'Запись успешно удалена!');

        return $this->redirect(['index']);
    }

    public function actionState($id)
    {
        $model = $this->findModel($id);
        $model->setStateStatus($model);

        return $this->redirect(['/task']);
    }

        /**
         * Finds the Task model based on its primary key value.
         * If the model is not found, a 404 HTTP exception will be thrown.
         * @param integer $id
         * @return Task the loaded model
         * @throws NotFoundHttpException if the model cannot be found
         */
        protected function findModel($id)
    {
        if (($model = Task::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException(Yii::t('app', 'ERROR_404'));
        }
    }



}
