<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title                   = 'Все задания';
$this->params['pageTitle']     = $this->title;
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-index box">

        <?php Pjax::begin(); ?>
    
    <div class="box-header">
        <?= Html::a('<i class="fa fa-plus"></i> Добавить', ['create'], ['class' => 'btn btn-primary']) ?>
    </div>
    <div class="box-body no-padding">
            <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            'id',
            'name',
//            'status',
            [
                'attribute' => 'status',
                'value' => function($data)
                {
                    return $data->status==1 ? 'Активирован' :'Остановлен';
                },
            ],

        [
            'class'          => 'common\widgets\ActionColumn',
            'firstButton'    => 'state',
            'hiddenButtons'  => ['update', 'hr', 'delete'],
            'buttons' => [
                'state' => function ($key, $model){
                    /* @var $model Users */
                    if ($model->status == 1){
                        return Html::a('<i class="fa fa-key"></i> Остановить', ['state', 'id' => $model->id], [
                            'data' => [
                                'method' => 'get',
                                'data-pjax' => '1',
                            ],
                            'class' => 'btn btn-warning btn-xs btn-flat'
                        ]);
                    }else{
                        return Html::a('<i class="fa fa-key"></i> Запустить', ['state', 'id' => $model->id], [
                            'data' => [
                                'method' => 'get',
                                'data-pjax' => '1',
                            ],
                            'class' => 'btn btn-success btn-xs btn-flat'
                        ]);
                    }
                }
            ],
            'buttonOptions'  => [
                'delete' => function ($model){
                    $title       = Yii::t('app', 'CONFIRM_TITLE');
                    $description = 'Вы уверены что хотите удалить данную запись?';

                    $options = [
                        'toggle'        => 'confirm',
                        'method'        => 'post',
                        'title'         => $title,
                        'description'   => $description,
                    ];

                    return ['data' => $options];
                },
            ]
        ]
        ],
        ]); ?>
        </div>
        <?php Pjax::end(); ?>
</div>