<?php

namespace common\modules\users\models;

use Yii;
use common\components\ParseDataInstagram;

/**
 * This is the model class for table "task".
 *
 * @property int $id
 * @property string $name
 * @property int $status
 */
class Task extends \yii\db\ActiveRecord
{

    const STATUS_ACTIVE = 1;
    const STATUS_STOP = 0;
    const WORKER_STOP = 2;
    const WORKER_DELETE = 3;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'task';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status'], 'integer'],
            ['status', 'default', 'value' => self::STATUS_STOP],
            ['status', 'in', 'range' => [self::STATUS_STOP, self::STATUS_ACTIVE]],
            [['name'], 'string', 'max' => 199],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'status' => 'Status',
        ];
    }

    public function sendParseProfile($name)
    {
            $parse = new ParseDataInstagram();
            $parse->name = $name;
            $parse->send();
    }


    public function setStateStatus($model)
    {
        if ($model->status == self::STATUS_ACTIVE)
        {
            $model->status = self::STATUS_STOP;
            $model->update(false);
            Yii::$app->redis->set($model->name, self::WORKER_STOP);
        }elseif ($model->status == self::STATUS_STOP)
        {
            $model->status = self::STATUS_ACTIVE;
            $model->update(false);
            $this->sendParseProfile($model->name);
            Yii::$app->redis->set($model->name, $model->status);
        }
    }
}
