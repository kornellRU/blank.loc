<?php

namespace common\components;

use yii;
use GearmanClient;

/**
 * Class SendEmail
 * @package common\components
 */
class ParseDataInstagram {
    /**
     * Название функции воркера
     */
    const WORKER_FUNCTION = 'parse-data-instagram';
    /**
     * Низкий приоритет
     */
    const PRIORITY_LOW = 'low';
    /**
     * Высокий приоритет
     */
    const PRIORITY_HIGHT = 'hight';
    /**
     * Кому
     * @var
     */
    public $name;

    /**
     * Отправляем письмо
     * @param string $priority
     */
    public function send($priority = self::PRIORITY_HIGHT)
    {
        $servers = Yii::$app->params['gearman']['servers']['master'];

        $client = new GearmanClient();
        $client->addServers($servers);

        if ($priority == self::PRIORITY_HIGHT){
            $client->doHighBackground(self::WORKER_FUNCTION, $this->getData());
        }else{
            $client->doLowBackground(self::WORKER_FUNCTION, $this->getData());
        }
    }

    /**
     * Получаем данные для отправки задачи на воркер
     * @return array
     */
    protected function getData()
    {
        $data = [
            'name'      => $this->name,
        ];

        return json_encode($data);
    }
}