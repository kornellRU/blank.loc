<?php

/* @var $this yii\web\View */
/* @var $model common\modules\users\models\Task */

$this->title = 'Create Task';
$this->params['pageTitle']     = 'Добавить новое задание';
$this->params['breadcrumbs'][] = ['label' => 'Все задания', 'url' => ['index']];
$this->params['breadcrumbs'][] = 'Добавить новое задание';
?>
<div class="task-create">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
