<?php

namespace console\workers;

use yii;
use\yii\httpclient\Client;
use common\modules\users\models\Task;
use yii\base\ErrorException;

/**
 * Class SendMail
 * @package console\models
 */
class ParseDataInstagram {
    /**
     * @param $job \GearmanJob
     */
    public function run($job)
    {
        $data = json_decode($job->workload(), true);
        while (true)
        {
            if ($this->getRedis($data['name']) == Task::STATUS_ACTIVE and !empty($data['name']))
            {
                $this->whileParse($data);
            }
        }
    }

    public function whileParse($data)
    {
        while ($parse = $this->parse($data))
        {
            $status = $this->getRedis($data['name']);

            $this->getVerifyStop($status, $data['name']);

            if (isset($parse['error']))
            {
                echo "\tError: " . $parse['error'] . "\n";
            }elseif(isset($parse) && $status==Task::STATUS_ACTIVE)
            {
                echo "Подписчиков:". $parse['edge_followed_by']."\nПодписок:". $parse['edge_followed']."\nПубликаций: ".$parse['count_posts']."\n\n";
            }elseif ($status==Task::WORKER_DELETE){
                echo "Задание удалено \n";
                break;
            }
            $this->verifySleep($data['name']);
        }
    }

    public function verifySleep($name)
    {
        for ($i=0; $i<=2; $i++){
            $this->getVerifyStop($this->getRedis($name), $name);
            sleep(1);
        }
    }

    public function getVerifyStop($status ,$name)
    {
        if ($status == Task::WORKER_STOP)
        {
            echo "Задание остановлено \n";
            $this->setRedis($name);
            die;
        }
    }

    public function getRedis($data)
    {
        return \Yii::$app->redis->get($data);
    }

    public function setRedis($key, $value = Task::STATUS_STOP)
    {
        Yii::$app->redis->set($key, $value);
    }


    /**
     * @param $data
     * @return mixed
     */
    public function parse($data)
    {
        try{
            $result=[];
            if (!$data['name']){
                return $result['error']='not name';
            }
            $client = new Client();
            $request = $client->createRequest()
                ->setMethod('get')
                ->setUrl('https://www.instagram.com/'.$data['name'])
                ->addHeaders(['user-agent' => 'Mozilla/5.0 (compatible; Googlebot/2.1; +http://www.google.com/bot.html)']) //прикинемся гугл ботом
                ->send();

            $arr = explode('window._sharedData = ', $request->content);
            $json = explode(';</script>', $arr[1]);
            $userArray = json_decode($json[0],true);
            if (empty($userArray))
            {
                return $result['error']='not json';
            }
            if ($userArray['entry_data']['ProfilePage'][0])
            {
                $result['edge_followed_by']=$userArray['entry_data']['ProfilePage'][0]['graphql']['user']['edge_followed_by']['count'];
                $result['edge_followed']=$userArray['entry_data']['ProfilePage'][0]['graphql']['user']['edge_follow']['count'];
                $result['count_posts']=$userArray['entry_data']['ProfilePage'][0]['graphql']['user']['edge_owner_to_timeline_media']['count'];
            }else {
                $result['edge_followed_by']= 0;
                $result['edge_followed']=0;
                $result['count_posts']=0;
            }

            return $result;

        }catch(ErrorException $e){
            echo 'Непредвиденная ошибка';
            die;
        }

    }
}