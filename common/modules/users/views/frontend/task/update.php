<?php

/* @var $this yii\web\View */
/* @var $model common\modules\users\models\Task */

$this->title = 'Update Task: ' . $model->name;

$this->params['pageTitle']     = 'Редактировать задание';
$this->params['breadcrumbs'][] = ['label' => 'Все задания', 'url' => ['index']];
$this->params['breadcrumbs'][] = $model->name;
?>
<div class="task-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
